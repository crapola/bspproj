function NodeView(name,pos)
{
	this.name=name;
	this.position=pos;
	this.left;
	this.right;
	this.draw=function(ctx)
	{
		// https://stackoverflow.com/questions/9644044/javascript-this-pointer-within-nested-function
		var myself=this;
		function drawSubNode(n)
		{
			if (n!=null)
			{
				ctx.strokeStyle="dodgerblue"
				ctx.beginPath();
				ctx.moveTo(myself.position.x,myself.position.y);
				ctx.lineTo(n.position.x,n.position.y);
				ctx.stroke();
				n.draw(ctx,n.position.x,n.position.y);
			}
		}
		ctx.fillText(this.name,this.position.x-5,this.position.y+5);
		drawSubNode(this.left);
		drawSubNode(this.right);
	}
}

// Converts a Node tree into a NodeView tree, set position of root.
function convertTree(n,x,y)
{
	if (n==null) return null;
	var c=new NodeView(n.data.wall.name,new Point(x,y));
	c.left=convertTree(n.subNodes.front,x-20,y+20);
	c.right=convertTree(n.subNodes.back,x+20,y+20);
	return c;
}

// https://llimllib.github.io/pymag-trees/
var i=0;
function knuthLayout(tree,depth)
{
	if (tree.left!=null)
		knuthLayout(tree.left,depth+1);
	tree.position.x=20+i*25;
	tree.position.y=20+depth*20;
	i++;
	if (tree.right!=null)
		knuthLayout(tree.right,depth+1);
}

function treeView(rootNode)
{
	this.ctx;
	if (this.ctx==null)
	{
		this.ctx=document.getElementById("graph").getContext("2d");
	}
	var c=convertTree(rootNode,200,20);
	knuthLayout(c,0);
	c.draw(ctx);
}
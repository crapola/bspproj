// Float comparison with epsilon.
function floatEqual(a,b)
{
	var epsilon=0.0001;
	return (Math.abs(a-b)<epsilon);
}
// -----------------------------------------------------------------------------
function Point(x,y)
{
	this.x=x||0;
	this.y=y||0;
	this.lengthSquared=function()
	{
		return x*x+y*y;
	}
	this.length=function()
	{
		return Math.sqrt(this.lengthSquared());
	}
	this.normalize=function()
	{
		var l=this.length();
		if (l==0) return;
		this.x/=l;
		this.y/=l;
	}
	this.equals=function(other)
	{
		return this.x==other.x && this.y==other.y;
	}
	this.rotate=function(angle)
	{
		var x2=this.x*Math.cos(angle)-this.y*Math.sin(angle);
		var y2=this.x*Math.sin(angle)+this.y*Math.cos(angle);
		this.x=x2;
		this.y=y2;
	}
}
function pointAdd(a,b)
{
	return new Point(a.x+b.x,a.y+b.y);
}
function pointSubstract(m,sub)
{
	return new Point(m.x-sub.x,m.y-sub.y);
}
function pointDivideByScalar(point,div)
{
	return new Point(point.x/div,point.y/div);
}
function pointMultiplyByScalar(point,mul)
{
	return new Point(point.x*mul,point.y*mul);
}
function pointDotProduct(a,b)
{
	return a.x*b.x+a.y*b.y;
}
// 3D cross product where Z=0.
function pointCrossProduct(a,b)
{
	return a.x*b.y-a.y*b.x;
}
// -----------------------------------------------------------------------------
function Edge(ax,ay,bx,by)
{
	this.a=new Point(ax,ay);
	this.b=new Point(bx,by);
	this.draw=function(ctx)
	{
		ctx.beginPath();
		ctx.moveTo(this.a.x,this.a.y);
		ctx.lineTo(this.b.x,this.b.y);
		ctx.stroke();
	}
	this.drawNormal=function(ctx,size)
	{
		var c=this.center();
		var ne=this.normal();
		var a1=new Point(ne.x,ne.y);
		var a2=new Point(ne.x,ne.y);
		a1.rotate(2.5);
		a2.rotate(-2.5);

		ne=pointMultiplyByScalar(ne,size);
		a1=pointMultiplyByScalar(a1,size/2);
		a2=pointMultiplyByScalar(a2,size/2);

		ne=pointAdd(ne,c);
		a1=pointAdd(a1,ne);
		a2=pointAdd(a2,ne);

		ctx.beginPath();
		ctx.moveTo(c.x,c.y);
		ctx.lineTo(ne.x,ne.y);
		ctx.lineTo(a1.x,a1.y);
		ctx.moveTo(ne.x,ne.y);
		ctx.lineTo(a2.x,a2.y);
		ctx.stroke();
	}
	// Returns normalized Point parallel to the edge, pointing from a to b.
	this.direction=function()
	{
		var p=pointSubstract(this.b,this.a);
		p.normalize();
		return p;
	}
	// Returns clockwise normalized normal Point.
	this.normal=function()
	{
		var p=this.direction();
		return new Point(-p.y,p.x);
	}
	/*
		Returns:
		 1 when point in front (towards normal).
		 0 when point on line.
		-1 when behind.
	*/
	this.facing=function(x,y)
	{
		var norm=this.normal();
		var d=pointSubstract(new Point(x,y),this.a);
		var dot=pointDotProduct(norm,d);
		return dot>0?1:dot<0?-1:0;
	}
	// Returns center of ab segment.
	this.center=function()
	{
		var sub=pointSubstract(this.b,this.a);
		return new Point(this.a.x+sub.x/2,this.a.y+sub.y/2);
	}
	// Returns an Edge orthogonal to this, starting at center, of length len.
	this.normalEdge=function(len)
	{
		var c=this.center();
		var n=this.normal();
		return new Edge(c.x,c.y,c.x+n.x*len,c.y+n.y*len);
	}
	// Returns true if equal to other.
	this.equals=function(other)
	{
		return this.a.equals(other.a)&&this.b.equals(other.b);
	}
}
/*
https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
a,b are Edges treated like infinite lines.
Return intersection Point or null.
*/
function lineIntersection(edgeA,edgeB)
{
	var p=edgeA.a;
	var q=edgeB.a;
	var r=edgeA.direction();
	var s=edgeB.direction();
	
	var rxs=pointCrossProduct(r,s);
	var qmpxr=pointCrossProduct(pointSubstract(q,p),r);
	// Colinear.
	if (rxs==0 && qmpxr==0)
	{
		console.log("Colinear");
		return null;
	}
	// Parallel.
	if (rxs==0 && qmpxr!=0)
	{
		console.log("Parallel");
		return null;
	}
	// u=(q−p)×r/(r×s)
	var u=pointCrossProduct(pointSubstract(q,p) , pointDivideByScalar(r, pointCrossProduct(r,s)));
	// result=q+u*s;
	var result=pointAdd(q,pointMultiplyByScalar(s,u));
	return result;
}

/*
Splits edge with infinite line, return original splitted if no cut, or two new
edges as {a,b}.
a is the edge facing 'cutting' (side of its normal), b is the other.
*/
function edgeLineSplit(splitted,cutting)
{
	var i=lineIntersection(splitted,cutting);
	if (i==null) return {a:splitted,b:null};
	else
	{
		var front,back;
		var e1=new Edge(splitted.a.x,splitted.a.y,i.x,i.y);
		var e2=new Edge(i.x,i.y,splitted.b.x,splitted.b.y);
		if (cutting.facing(splitted.a.x,splitted.a.y)==1)
		{
			front=e1;back=e2;
		}
		else
		{
			front=e2;back=e1;
		}
		return {a:front,b:back}
	}
}

// Returns true if point on segment.
function pointOnEdge(point,edge)
{
	// Check if point is one of extremities.
	if (point.equals(edge.a) || point.equals(edge.b)) return true;
	// Otherwise do the test.
	var v1=pointSubstract(point,edge.a);
	var l1=v1.length();
	var v2=pointSubstract(edge.b,edge.a);
	var l2=v2.length();
	v1.normalize();
	v2.normalize();
	if (!floatEqual(v1.x,v2.x) || !floatEqual(v1.y,v2.y)) return false;
	if (l1>l2) return false;
	return true;
}

function edgeFacing(tested,cutting)
{
	return cutting.facing(tested.a.x,tested.a.y)&&cutting.facing(tested.b.x,tested.b.y);
}

function Polygon(edgeList)
{
	this.edges=edgeList||[];
	// Returns Point centroid.
	this.centroid=function()
	{
		var ax=0,ay=0;
		this.edges.forEach(function(e)
		{
			ax+=e.a.x;
			ay+=e.a.y;
		});
		var edgeCount=this.edges.length;
		var center=new Point(ax/edgeCount,ay/edgeCount);
		return center;
	}
	this.draw=function(ctx)
	{
		this.edges.forEach(function(e,i)
		{
			ctx.fillText(i,e.a.x+5,e.a.y+5);
			e.draw(ctx);
		});
	};
	this.drawFilled=function(ctx)
	{
		ctx.beginPath();
		ctx.moveTo(this.edges[0].a.x,this.edges[0].a.y);
		this.edges.forEach(function(e,i)
		{
			//ctx.fillText(i,e.a.x+5,e.a.y+5);
			ctx.lineTo(e.b.x,e.b.y);
		});
		ctx.fill();
	}
}

// Returns: a:Poly on normal side, b:Other poly.
function polygonLineSplit(poly,cut)
{
	var pl=new Polygon;
	var pr=new Polygon;
	var vl=[];
	var vr=[];
	poly.edges.forEach(function(e)
	{
		var pa=e.a;
		var pb=e.b;
		var facingA=cut.facing(pa.x,pa.y);
		var facingB=cut.facing(pb.x,pb.y);

		if (facingA>=0) // Entirely in front.
		{
			vl.push(e.a);
		}
		if (facingA<1) // Entirely behind;
		{
			vr.push(e.a);
		}
		if (facingA+facingB==0)
		{
			var pi=lineIntersection(e,cut);
			vl.push(pi);
			vr.push(pi);
		}		
	});

	if (vl.length>0)
	{
	for (var i=0;i<vl.length-1;i++)
	{
		var e=new Edge(vl[i].x,vl[i].y , vl[i+1].x,vl[i+1].y);
		pl.edges.push(e);
	}	
	pl.edges.push( new Edge(vl[vl.length-1].x,vl[vl.length-1].y,vl[0].x,vl[0].y));
	}

	if (vr.length>0)
	{
	for (var i=0;i<vr.length-1;i++)
	{
		var e=new Edge(vr[i].x,vr[i].y , vr[i+1].x,vr[i+1].y);
		pr.edges.push(e);
	}
	// close
	pr.edges.push( new Edge(vr[vr.length-1].x,vr[vr.length-1].y,vr[0].x,vr[0].y));
	}
	return {a:pl,b:pr};
}
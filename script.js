function Wall(ax,ay,bx,by,name)
{
	this.name=name;
	var edge=new Edge(ax,ay,bx,by);
	this.edge=function()
	{
		return edge;
	}
	this.draw=function(ctx)
	{
		ctx.lineWidth=3;
		ctx.strokeStyle="black";
		edge.draw(ctx);
		ctx.lineWidth=1;
		ctx.strokeStyle="blue";
		edge.drawNormal(ctx,20);
	}
}

function NodeData(w)
{
	this.wall=w;	// Cutting wall.
	this.polygon;	// Polygon including wall.
}

function Node()
{
	this.data;
	this.subNodes={front:null,back:null};

	this.create=function(wallList,poly)
	{
		console.log("Create node with ");
		console.log(wallList);
		var f_walls=[];
		var b_walls=[];
		if (wallList.length==0)
		{
			console.log("No walls");
			return null;
		}
		var base=wallList[0];
		this.createNodeData(base);
		this.data.polygon=poly;
		var walls=wallList.slice(1);
		walls.forEach(function(w)
		{
			var pa=w.edge().a;
			var pb=w.edge().b;
			var calc=this.data.wall.edge().facing(pa.x,pa.y)+this.data.wall.edge().facing(pb.x,pb.y);
			if (calc==2) // Entirely in front.
			{
				console.log(w.name+" is in front of "+this.data.wall.name);
				f_walls.push(w);
			}
			else
			{
				if (calc==-2) // Entirely behind.
				{
					console.log(w.name+" is behind "+this.data.wall.name);
					b_walls.push(w);
				}
				else
				{
					if (this.data.wall.edge().normal()==w.edge().normal())
					{
						// Coincident. TODO
						console.log(w.name+" is on "+this.data.wall.name);
						f_walls.push(w);
					}
					else
					{
						// Spanning.
						console.log(w.name+" is split by "+this.data.wall.name);
						var splits=edgeLineSplit(w.edge(),this.data.wall.edge());
						var e1=splits.a;
						var e2=splits.b;
						var w1=new Wall(e1.a.x,e1.a.y,e1.b.x,e1.b.y,w.name+"_1");
						var w2=new Wall(e2.a.x,e2.a.y,e2.b.x,e2.b.y,w.name+"_2");
						console.log("Created splits "+w1.name+" and "+w2.name);
						// Determine which split is front.
						var l,r;
						l=w1;
						r=w2;
						console.log("We'll add L="+l.name+" R="+r.name);
						f_walls.push(l);
						b_walls.push(r);
					}
				}
			}
		},this);
		console.log("Walls around "+this.data.wall.name+":");
		console.log(f_walls);
		console.log(b_walls);
		// Recurse.
		if (f_walls.length>0)
		{
			this.subNodes.front=new Node;
			var p=polygonLineSplit(this.data.polygon,this.data.wall.edge());
			this.subNodes.front.create(f_walls,p.a);
		}
		if (b_walls.length>0)
		{
			this.subNodes.back=new Node;
			var p=polygonLineSplit(this.data.polygon,this.data.wall.edge());
			this.subNodes.back.create(b_walls,p.b);
		}
	}
	this.createNodeData=function(w)
	{
		this.data=new NodeData(w);
		console.log("Created node "+w.name);
	}
	this.left=function()
	{
		return this.subNodes.front;
	}
	this.right=function()
	{
		return this.subNodes.back;
	}
}

function getLeafAt(root,point)
{
	var node=root;
	var debugStr="getLeafAt";
	while (true)
	{
		debugStr+="->"+node.data.wall.name;
		//var d=pointDotProduct(point,node.data.edge().normal())-0.0;
		var d=node.data.wall.edge().facing(point.x,point.y);
		//console.log(node.data.name+" "+d);
		if (node.subNodes.front!=null && d>=0)
	{
			node=node.subNodes.front;debugStr+="f";
	}
		else if (node.subNodes.back!=null && d<0)
			node=node.subNodes.back;
		else break;

		
	}
	console.log(debugStr);
	return node;
}

function repaint(ctx,root,mx,my)
{
	ctx.clearRect(0,0,500,500);

	// Draw space based on BSP
	breadthFirst(root,function(node)
	{
		var e=node.data.wall.edge();
		// Edge
		ctx.lineWidth=2;
		ctx.strokeStyle="green";
		e.draw(ctx);
		// Point
		ctx.beginPath();
		ctx.fillStyle="black";
		ctx.arc(e.a.x,e.a.y,5,0,2*Math.PI,false);
		ctx.fill();
		ctx.fillText(node.data.wall.name,e.a.x+10,e.a.y+10);
		ctx.strokeStyle="blue";
		// Normal
		ctx.lineWidth=1;
		e.drawNormal(ctx,10);
	});

	// Highlight dropdown node.
	var name=document.getElementById("select").value;
	var node=findNodeCalled(root,name);
	if (node)
	{
		ctx.fillStyle="#FF000070";
		node.data.polygon.drawFilled(ctx);	
	}

	// Highlight pointed by mouse.
	if (!mx) return;
	mp=new Point(mx,my);
	ctx.fillStyle="black";
	mp.x=mx;
	mp.y=my;
	var n=getLeafAt(root,mp);
	ctx.fillText(mp.x+","+mp.y+" "+n.data.wall.name,15,450);
	ctx.fillStyle="#FFAA0080";
	n.data.polygon.drawFilled(ctx);	
}

function fillDropdown(tree)
{
	// Build lise of names.
	var names=[];
	breadthFirst(tree,function(node)
	{
		names.push(node.data.wall.name);
	});
	// Add them to dropdown.
	var select=document.getElementById("select");
	names.forEach(function(n)
	{
		var e=document.createElement("option");
		e.textContent=n;
		e.value=n;
		select.add(e);
	});
}

// Get node by name.
function findNodeCalled(tree,str)
{
	var returned=null;
	breadthFirst(tree,function(node)
	{
		if (str===node.data.wall.name)
		{
			returned=node;
		}
	});
	return returned;
}

// Called when drop down list is used.
function onSelect()
{
	console.log("Yo");
}

function main()
{
	var canvas = document.getElementById("myCanvas");
	var ctx = canvas.getContext("2d");

	ctx.font="20px Georgia";
	ctx.linecap="round";

	var walls=[
		new Wall(180,200,350,200,"A"),
		new Wall(180,150,180,250,"B"),
		new Wall(380,300,450,120,"C"),
		new Wall(20,50,200,400,"D"),
		//new Wall(140,300,410,10,"E"),
		new Wall(130,310,620,100,"F")
	];

	var borderPoly=new Polygon([
	new Edge(0,0,500,0),
	new Edge(500,0,500,500),
	new Edge(500,500,0,500),
	new Edge(0,500,0,0),
	]);

	// Build tree
	var root=new Node();
	root.create(walls,borderPoly);

	// Draw tree graph
	treeView(root);

	// Test
	runTests();

	fillDropdown(root);

	// =========================================================================

	function getMousePos(canvas,evnt)
	{
		var rect = canvas.getBoundingClientRect();
		return {
		  x: evnt.clientX - rect.left,
		  y: evnt.clientY - rect.top
		};
	  }

	var mp=new Point;
	canvas.addEventListener('mousemove',function(e)
	{
		var rect = this.getBoundingClientRect();
		var y=450;
		ctx.clearRect(0,y-60,300,y+60);
		//ctx.fillRect(0,0,200,100);
		var mx=getMousePos(this,e).x;
		var my=getMousePos(this,e).y;
		repaint(ctx,root,mx,my);
	});

	canvas.dispatchEvent(new Event('mousemove'));
}
// https://stackoverflow.com/questions/7459422/best-way-to-implement-a-queue-in-javascript
function Queue()
{
	var items;
	this.enqueue = function(item)
	{
		if (typeof(items) === 'undefined')
		{
			items=[];
		}
		items.push(item);
	}
	this.dequeue = function()
	{
		return items.shift();
	}
	this.peek = function()
	{
		return items[0];
	}
	this.empty=function()
	{
		return items.length==0;
	}
}

/*
Breadth first traversal
https://en.wikipedia.org/wiki/Tree_traversal
*/
function breadthFirst(root,visitor)
{
	var q=new Queue;
	q.enqueue(root);
	while(!q.empty())
	{
		var n=q.dequeue();
		visitor(n);
		if (n.left()!=null)
			q.enqueue(n.left());
		if (n.right()!=null)
			q.enqueue(n.right());
	}
}
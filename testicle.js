function test(input,expected)
{
	if (input!=expected)
	{
		console.log("Test failed!");
		console.log({result:input,expected:expected});
	}
	else
		console.log("Test OK.");
}

function test_pointOnEdge()
{
	var p1=new Point(-10,0);
	var p2=new Point(0,0);
	var p3=new Point(5,0);
	var p4=new Point(10,0);
	var e=new Edge(5,0,10,0);
	var e2=new Edge(0,0,100,50);
	var p5=new Point(25,12.5);
	test(pointOnEdge(p1,e),false);
	test(pointOnEdge(p2,e),false);
	test(pointOnEdge(p3,e),true);
	test(pointOnEdge(p4,e),true);
	test(pointOnEdge(p5,e2),true);
}

function test_lineIntersection()
{
	var eA=new Edge(20,20 , 110,65);
	var eB=new Edge(50,90 , 50,10);
	var eC=new Edge(80,70 , 110,85);
	var eD=new Edge(120,70 , 140,80);
	var eE=new Edge(85,15 , 65,55);
	var pAB=new Point(50,35);
	var pAE=new Point(70,45);
	test( lineIntersection(eA,eB).x , pAB.x );
	test( lineIntersection(eA,eB).y , pAB.y );
	test( lineIntersection(eA,eE).x , pAE.x );
	test( lineIntersection(eA,eE).y , pAE.y );
	test( lineIntersection(eA,eC) , null );
	test( lineIntersection(eA,eD) , null );
}

function test_edgeLineSplit()
{
	var eA=new Edge(20,20 , 110,65);
	var eB=new Edge(50,90 , 50,10);
	var eC=new Edge(80,70 , 110,85);
	var eD=new Edge(120,70 , 140,80);
	var eE=new Edge(85,15 , 65,55);
	var s;
	// A+B
	var sAB1=new Edge(20,20,50,35);
	var sAB2=new Edge(50,35,110,65);
	s=edgeLineSplit(eA,eB);
	test(sAB1.equals(s.b),true);
	test(sAB2.equals(s.a),true);
	// A+E
	var sAE1=new Edge(70,45,110,65);
	var sAE2=new Edge(20,20,70,45);
	s=edgeLineSplit(eA,eE);
	test(sAE1.equals(s.b),true);
	test(sAE2.equals(s.a),true);
	// A+C
	s=edgeLineSplit(eA,eC);
	test(s.a.equals(eA),true);
	test(s.b,null);
	// A+D
	s=edgeLineSplit(eA,eC);
	test(s.a.equals(eA),true);
	test(s.b,null);
}

function runTests()
{
	test_pointOnEdge();
	test_lineIntersection();
	test_edgeLineSplit();
}